<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>

    <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            display: table;
            font-weight: 100;
            font-family: 'Lato';
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 96px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        @if ($user)
            <p>{{!empty($user->name) ? $user->name : $user->email}} - <a href="/logout">Logout</a></p>
        @else
            <p>
                {{ $errors->first('email') }}
                {{ $errors->first('password') }}
            </p>
            <fieldset>
                <legend>Login</legend>
                {!! Form::open(array('url' => '/login')) !!}
                <p>
                    {!! Form::label('email', 'Email Address') !!}
                    {!! Form::text('email') !!}
                </p>
                <p>
                    {!! Form::label('password', 'Password') !!}
                    {!! Form::password('password') !!}
                </p>
                <p>{!! Form::submit('Login') !!}</p>
                {!! Form::close() !!}
            </fieldset>

            <fieldset>
                <legend>Register</legend>
                {!! Form::open(array('url' => '/register')) !!}
                <p>
                    {!! Form::label('name', 'Name') !!}
                    {!! Form::text('name') !!}
                </p>
                <p>
                    {!! Form::label('email', 'Email Address') !!}
                    {!! Form::text('email') !!}
                </p>
                <p>
                    {!! Form::label('password', 'Password') !!}
                    {!! Form::password('password') !!}
                </p>
                <p>
                    {!! Form::label('password_confirmation', 'Confirm Password') !!}
                    {!! Form::password('password_confirmation') !!}
                </p>
                <p>{!! Form::submit('Register') !!}</p>
                {!! Form::close() !!}
            </fieldset>
        @endif
    </div>
</div>
</body>
</html>
